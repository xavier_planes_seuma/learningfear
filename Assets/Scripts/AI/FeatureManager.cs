﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeatureManager{

    private int visionColisions;

    //input list

    public FeatureManager()
    {
        //this.featuresDictionary = new Dictionary<string, Feature>();
    }

    public int getVC()
    {
        return visionColisions;
    }

    public void Increase()
    {
        visionColisions++;
    }

    public void Reset()
    {
        visionColisions = 0;
    }
}
