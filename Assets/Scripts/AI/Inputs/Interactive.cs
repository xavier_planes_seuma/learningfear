﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactive : MonoBehaviour
{
    [SerializeField] private string inputName;

    public string InputName {
        get { return inputName; }
        private set { inputName = value; }
    }

    public int Times { get; protected set; }
    public float Duration { get; protected set; }
    protected float AuxTime { get; set; }
   
    public void ResetValues()
    {
        Times = 0;
        Duration = 0f;
    }
}
