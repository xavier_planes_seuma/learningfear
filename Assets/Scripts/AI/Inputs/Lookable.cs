﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lookable : Interactive {

    private void Start()
    {
        print(InputName);
    }

    private void OnMouseEnter()
    {
        print(InputName + "enter");
        Times++;
        AuxTime = Time.time;
    }

    private void OnMouseExit()
    {
        Duration += Time.time - AuxTime;
        print(InputName + ": " + Duration + "  " + Times);
    }

    //private void OnMouseOver()
    //{
    //    print("over");
    //}

}
