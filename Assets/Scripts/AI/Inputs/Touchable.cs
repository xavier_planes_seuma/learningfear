﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Touchable : Interactive {

    public void OnMouseDown()
    {
        print("down");
        Times++;
        AuxTime = Time.time;
    }

    private void OnMouseUp()
    {
        print("up");
        Duration = Time.time - AuxTime;
    }

    private void OnMouseUpAsButton()
    {
        print("click");
    }

    private void OnMouseDrag()
    {
        print("drag");
    }
}
