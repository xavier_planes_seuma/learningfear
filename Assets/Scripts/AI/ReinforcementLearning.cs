﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReinforcementLearning : MonoBehaviour{

    //private FeatureManager lastFeature;
    //[SerializeField] private List<Inputs> inputs;

    private readonly List<Interactive> lastRoominputs;

    public List<Interactive> LastRoomInputs
    {
        private get; set;
    }

    // To start we can keep track the time here.
    // Maybe later we can create a "TimeInput" class or something.
    private float startTime;

    public void OnRoomEnter(List<Interactive> roomInputs)
    {
        //foreach (Inputs input in inputs)
        //{
        //    input.ResetInputValues();
        //}
        LastRoomInputs = roomInputs;

        foreach (Interactive inter in LastRoomInputs)
        {
            inter.ResetValues();
        }

        startTime = Time.time;
    }

    public void OnRoomExit()
    {
        //foreach(Inputs input in inputs)
        //{
        //    foreach (KeyValuePair<string, float> entry in input.InputValuesDictionary)
        //    {
        //        Debug.Log(entry.Key + " : " + entry.Value);
        //    }
        //}

        foreach (Interactive inter in LastRoomInputs)
        {
            print("Name: " + inter.InputName+ " times: " + inter.Times+ " total duration: " + inter.Duration);
        }

        //Debug.Log("roomTime : " + (Time.time - startTime));
    }
}
