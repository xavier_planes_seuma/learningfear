﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomController : MonoBehaviour {

    [SerializeField]
    private List<Interactive> roomInputs;

    public readonly string PLAYER_TAG = "Player";

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PLAYER_TAG) && other.GetComponentInChildren<ReinforcementLearning>() != null)
        {
            ChangeLight(other);
            ReinforcementLearning RL = other.GetComponentInChildren<ReinforcementLearning>();
            RL.OnRoomEnter(roomInputs);
        }
       
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(PLAYER_TAG) && other.GetComponentInChildren<ReinforcementLearning>() != null)
        {
            ChangeLight(other);
            ReinforcementLearning RL = other.GetComponentInChildren<ReinforcementLearning>();
            RL.OnRoomExit();
        }
    }

    private void ChangeLight(Collider other)
    {
        Light light = other.GetComponentInChildren<Light>();
        light.enabled = !light.enabled;
    }

}
