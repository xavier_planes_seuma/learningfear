﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creator : MonoBehaviour {

    [SerializeField] private List<GameObject> forms;

	// Use this for initialization
	void Start () {
        //forms = new List<GameObject>();

        System.Random rand = new System.Random();

        foreach(GameObject go in forms)
        {
            int value = rand.Next(0, 2);
            go.SetActive(value == 0);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
