﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactor : MonoBehaviour
{

    public void OnMouseDown()
    {
        print("down");
    }

    private void OnMouseUp()
    {
        print("up");
    }

    private void OnMouseUpAsButton()
    {
        print("click");
    }

    private void OnMouseEnter()
    {
        print("enter");
    }

    private void OnMouseExit()
    {
        print("exit");
    }

    //private void OnMouseOver()
    //{
    //    print("over");
    //}

    private void OnMouseDrag()
    {
        print("drag");
    }

    
}