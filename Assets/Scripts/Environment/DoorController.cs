﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour {

    private bool isOpen = false;

    private void OnMouseUpAsButton()
    {
        if (!GetComponent<Animation>().isPlaying)
        {
            if (!isOpen)
            {
                GetComponent<Animation>().Play("OpenDoor");
            }
            else
            {
                GetComponent<Animation>().Play("CloseDoor");
            }
            isOpen = !isOpen;
        }
    }
}
