﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inputs : MonoBehaviour {

    //protected FeatureManager featureManager;
    private Dictionary<string, float> inputValuesDictionary;

    public Dictionary<string, float> InputValuesDictionary
    {
        get; protected set;
    }

    protected void Start()
    {
        InputValuesDictionary = new Dictionary<string, float>();
    }

    public void ResetInputValues()
    {
        List<string> keys = new List<string>(InputValuesDictionary.Keys);
        foreach (string key in keys)
        {
            InputValuesDictionary[key] = 0;
        }
    }
}
