﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InteractionSystem : MonoBehaviour {

    private RaycastHit hit;
    private bool hasTarget = false;

    [SerializeField]
    private GameObject currentTarget;
    private GameObject dummy;

    void Start () {
        currentTarget.transform.position = new Vector3(0, 0, 0);
        dummy = currentTarget;
    }

    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        if (Physics.Raycast(ray, out hit, 2) && hit.collider.gameObject.GetComponent<EventTrigger>() != null)
        {

            if (!currentTarget.Equals(hit.collider.gameObject))
            {
                currentTarget = hit.collider.gameObject;
                hasTarget = true;
                //reticle.SetPointerTarget(hit.collider.GetComponent<Transform>().position, true);
                ExecuteEvents.Execute<IPointerEnterHandler>(currentTarget, new PointerEventData(EventSystem.current), ExecuteEvents.pointerEnterHandler);
            }

        }
        else if (hasTarget)
        {
            ExecuteEvents.Execute<IPointerExitHandler>(currentTarget, new PointerEventData(EventSystem.current), ExecuteEvents.pointerExitHandler);
            //reticle.SetPointerTarget(currentTarget.GetComponent<Transform>().position, false);
            currentTarget = dummy;
            hasTarget = false;

        }
    }
}
