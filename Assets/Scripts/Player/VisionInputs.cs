﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionInputs : Inputs{

    private RaycastHit hit;

    private readonly int PLAYER_SIGHT = 10;

    // Overrides the parent method Start()
    new void Start()
    {
        base.Start();
        InputValuesDictionary.Add("vc", 0);
        InputValuesDictionary.Add("terrainCounter", 0);
    }

    // Update is called once per frame
    void Update () {
        Vector3 fwd = GetComponent<Transform>().TransformDirection(Vector3.forward);
        RaycastHit hit;

        if (Physics.Raycast(GetComponent<Transform>().position, fwd, out hit, PLAYER_SIGHT))
        {
            InputValuesDictionary["vc"] += 1;
            if (hit.collider.gameObject.CompareTag("Terrain"))
            {
                InputValuesDictionary["terrainCounter"] += 1;
            }
            //print("There is something in front of the object!");
        }
        else
        {
            //print("There is NOTHING!!!! in front of the object!");
        }         
    }

    
}
