# LearningFear

## Inputs classes

* VisionInputs: Gathers all the info about where is looking the player
* TimeInputs: Gathers all the info about how many time is spent by the player in some place
* InteractionInputs: Gathers all the info about the interaction of the player with the environment
* DecisionInputs: Gathers all the info about the player decisions (like how they open the doors)
* MechanicInputs: Gathers all the info about the usage of the mechanics (flashlight, inhaler...)
